$(document).ready(function () {
    $('.image.welcome').transition('pulse')

    $('#modalButton').click(function () {
        $('.ui.modal').modal('show');
        // init Masonry
        var $grid = $('#masonryGrid').masonry({
            // options...
            itemSelector: '.grid-item',
            isFitWidth: true,
            columnWidth: 1
        });
        // layout Masonry after each image loads
        $grid.imagesLoaded().progress(function () {
            $grid.masonry('layout');
        });
    });
});